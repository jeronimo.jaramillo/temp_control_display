#include <gui/screen1_screen/Screen1View.hpp>
#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


Screen1View::Screen1View()
{


}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View::open_door()
{
	door = !door;
	if( door == 1){
		HAL_GPIO_WritePin(Door_GPIO_Port,Door_Pin, GPIO_PIN_RESET);
	}else if(door == 0){
		HAL_GPIO_WritePin(Door_GPIO_Port,Door_Pin, GPIO_PIN_SET);
	}
	Unicode::snprintf(door_statusBuffer,DOOR_STATUS_SIZE,"%d",door);

	door_status.resizeToCurrentText();
	door_status.invalidate();
}

void Screen1View::updateTemp(float temp) {

	temp_stauts.resizeToCurrentText();
	temp_stauts.invalidate();

	temp = return_temp();

	Unicode::snprintfFloat(temp_stautsBuffer,TEMP_STAUTS_SIZE, "%.2f", temp);

	temp_stauts.resizeToCurrentText();
	temp_stauts.invalidate();


	if ((temp >= 45) || (temp <= 0)) {
		temp_ok.setVisible(false);
		temp_ok.invalidate();
		fan = 0;
		Unicode::snprintf(fan_statusBuffer, FAN_STATUS_SIZE, "%d", fan);
		fan_status.invalidate();


		temp_no_ok.setVisible(true);
		temp_no_ok.invalidate();

	} else {
		temp_ok.setVisible(true);
		temp_ok.invalidate();

		temp_no_ok.setVisible(false);
		temp_no_ok.invalidate();
	}

	if (temp < 10.0 && temp > 0.0) {

		heater = 1;
		Unicode::snprintf(heater_statusBuffer, HEATER_STATUS_SIZE, "%d", heater);
		heater_status.invalidate();

		fan = 0;  //PWM turn off (0%)
		Unicode::snprintf(fan_statusBuffer, FAN_STATUS_SIZE, "%d", fan);
		fan_status.invalidate();
		fan_status.resizeToCurrentText();
		fan_status.invalidate();

	} else if(temp>10){

		heater = 0;
		Unicode::snprintf(heater_statusBuffer, HEATER_STATUS_SIZE, "%d",heater);
		heater_status.invalidate();
	}

	if(temp > 20.0 && temp < 28.0){
		fan_status.resizeToCurrentText();
		fan_status.invalidate();

		fan = 20;  //PWM turn on (20%)
		Unicode::snprintf(fan_statusBuffer, FAN_STATUS_SIZE, "%d", fan);
		fan_status.invalidate();
		fan_status.resizeToCurrentText();
		fan_status.invalidate();

	}else if(temp > 30.0 && temp < 38.0){
		fan_status.resizeToCurrentText();
		fan_status.invalidate();

		fan = 60; //PWM turn on (60%)
		Unicode::snprintf(fan_statusBuffer, FAN_STATUS_SIZE, "%d", fan);
		fan_status.invalidate();
		fan_status.resizeToCurrentText();
		fan_status.invalidate();

	}else if(temp > 40.0 && temp < 45.0){
		fan_status.resizeToCurrentText();
		fan_status.invalidate();

		fan = 100;   //PWM turn on (100%)
		Unicode::snprintf(fan_statusBuffer, FAN_STATUS_SIZE, "%d", fan);
		fan_status.invalidate();
		fan_status.resizeToCurrentText();
		fan_status.invalidate();
	}


}


