Display LCD with TouchScreen

Author.
Jeronimo Jaramillo Bejarano
email: jeronimojaramillo.99@gmail.com

Objective. 

This project consists of the implementation of a state machine designed for the control of a cold room with 2 actuators:
a fan, and a heater. That visualizes the data on an LCD touchscreen display 240x320 pixels. The temperature is taken by the 
bmp208 sensor. And was implemented a touch button on the screen to open or close the door.

PLATFORM.
The board STM32F429ZI discovery 1 will be used on this development. 

FUNCTIONALITY.
The system has 2 parts, the part of the graphics of the LCD and the part of the structure of the states-machine of the system.
The main code has 2 tasks. In the Second Task, the SPI was implemented to use the sensor and start the states-machine by taking the measurement of the temperature and sending this value to the graphical part to show on the LCD.

The values of the actuators are: 

Heater --> 1: ON / 0:OFF
Door   --> 1: Close / 0: Open
Fan      --> 0% (OFF), 20%(ON), 60% (ON) and 100% (ON).


The states-Machine is the next: 

* if the temperature is greater than 12°C and less than 18°C, the heater and the fan are OFF. (COLD)
* if the temperature is greater than 20°C and less than 28°C, the heater is OFF and the fan is at 20% of the speed. (MILD)
* if the temperature is greater than 30°C and less than 38°C, the heater is OFF and the fan is at 60% of the speed. (HOT)
* if the temperature is greater than 40°C and less than 45°C, the heater is OFF and the fan is at 100% of the speed. (VERY HOT)
* if the temperature is greater than 45°C or less than 0°C, the heater and the fan are OFF. And show an image (RED dot) on the display LCD. (ALERT)
* if the button is pushed the door is opened and if pushed again the door is closed 

